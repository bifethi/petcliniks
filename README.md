# PetClinik

1- Install plugins on jenkins
    a- docker & docker pipeline

3- add jenkins to the VM group users
    a- ps aux | grep jenkins
    b- sudo usermod -aG docker jenkins
    c- sudo systemctl restart jenkins

2- add this code to the pom file

<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.5</version> <!-- ensure you use the latest version -->
    <executions>
        <execution>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>report</id>
            <phase>prepare-package</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
        <excludes>
            <exclude>sun/util/resources/cldr/provider/**</exclude>
            <exclude>java/**</exclude>
        </excludes>
    </configuration>
</plugin>